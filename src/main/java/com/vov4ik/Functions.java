package com.vov4ik;

/**
 * the Functions class contains all methods of this program.
 *
 */
class Functions {
    final static int FIRST_FIBONACCI_NUMBER = 1;
    /** The makeFibonacciSequence method takes different parameters
    * from user and build the Fibonacci sequence depends on this parameters
     */
    void makeFibonacciSequence(final int fibonacciSequenceCapacity) {
        int[] fibonacciSequence = new int[fibonacciSequenceCapacity];
        fibonacciSequence[0] = FIRST_FIBONACCI_NUMBER;
        fibonacciSequence[1] = FIRST_FIBONACCI_NUMBER;
        /**
         * This loop makes an array of Fibonacci numbers
         */
        for (int i = 2; i < fibonacciSequenceCapacity; i++) {
            fibonacciSequence[i] = fibonacciSequence[i - 1]
                                + fibonacciSequence[i - 2];
        }
        for (int i = 0; i < fibonacciSequenceCapacity; i++) {
            System.out.print(fibonacciSequence[i] + " ");
        }
    }
}
