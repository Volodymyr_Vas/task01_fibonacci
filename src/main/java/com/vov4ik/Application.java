/*
 * Fibonacci class
 *
 * version 1.0.0
 *
 * created by Volodymyr Vasylyshyn
 *
 * date: 01.04.2019
 *
 * All rights unreserved (yet)
 *
 */
package com.vov4ik;

/**
 * @author Volodymyr Vasylyshyn
 * @version 1.0.2
 * @since 01.04.2019
 *
 */

import java.util.Scanner;

public class Application {
    /**
     * This is start point of program.
     */
    public static void main(String[] args) {
        int fibonacciSequenceSize;
        System.out.println("Please, enter the size of Fibonacci sequence:");
        do {
            Scanner input = new Scanner(System.in);
            try {
                fibonacciSequenceSize = input.nextInt();
            } catch (Exception e) {
                System.out.println("Your input is incorrect, please try again");
                continue;
            }
            if (fibonacciSequenceSize > 2) {
                break;
            } else {
                System.out.println("Your input is incorrect, please try again");
            }
        } while (true);
        new Fibonacci(fibonacciSequenceSize);
    }
}
