package com.vov4ik;

/**
 * The Fibonacci class.
 * It takes input from user and makes a Fibonacci sequence
 */
class Fibonacci {
    /**
    * This variable means min size of Fibonacci sequence
     */
    final static int MIN_SEQUENCE_LONG = 3;
    /**
    * This constructor takes 1 variable
     * and makes a Fibonacci sequence from 1 to n
     */
    Fibonacci(final int numbersOfSequence) {
        Functions functions = new Functions();
        if (numbersOfSequence < MIN_SEQUENCE_LONG) {
            System.out.println("Size of Fibonacci sequence"
                            + "can't be less than 3 numbers, "
                            + "please write the correct number");
        } else {
            functions.makeFibonacciSequence(numbersOfSequence);
        }
    }
}
